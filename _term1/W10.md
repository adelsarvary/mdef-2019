---
title: Designing with Collective Intelligence
layout: "page"
order: 10
---


![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6b55083f13fe-Joler.jpg)

### Faculty
Ramon Sangüesa  
Lucas Peña 

### Syllabus and Learning Objectives
The course explores the development of artificial intelligence and its close connection with design from its very beginning.
We will delve into the concepts of design that exist in AI and will connect them with their implications and their possibilities as guidelines for emergent design.
In this process we will explore: the autonomization of the object, the collective dimension of intelligent behaviour and the challenges that they pose for established design methods.  

### Total Duration
Classes: Mon-Thu (3-6pm) (12 per week)  
Student work hours: 12 per week  
Total hours per student: 24 per week  

### Structure and Phases
*First day: About Automated and Extended Intelligence*  
Goals of the day:   
-	Question ideas about intelligence, human centeredness of intelligence, automation but also the Silicon Valley hype.  Agency. 
-	Main topics: Intelligence, AI, AI history, AI as a wealth of technics, power, implications for de-sign  

1.	Presentation of professors and course
2.	Group activity: What is intelligence?
3.	How to automate intelligence? Symbol Grounding
4.	Group activity: Searle vs. Turing
5.	Advancing with no consensus about Intelligence: The Design Way (12 white men in Dart-mouth)
6.	Ai and Cybernetics
7.	The rational agent) 
8.	IOT-BIG DATA-ML-IA
9.	Inteligent Objects: from Roomba to Google Clips Camera (
10.	Group Activity: Define your intelligent object 
11.	Summing up: what problems in defining intelligent objects? What is your role? How do you control the evolution of your intelligent object? Can you design your object disconnected from the world/context/other intelligent objects/people/culture? How we can create a method to design intelligent objects?

Some takeaways: intelligent agent, current IA paradigm (Rational Agents)  

*Second day: Designing Intelligent Things*  
Goals of the day:   
-	Think about designing intelligent objects as a networked approach
-	Main topics: Intelligent Products, Metaproducts, Network-based AI design, AI design as Multi-agent Design, the user as an Agent and part of the network, a sketch of a methods
-	Some take aways: networked approach to design, a hint of posthuman/postidigital design
-	Final question: how to design intelligent systems that evolve (preparing for Machine Learning)

1.	Reminder of previous day): main concepts, their “intelligent objects”
2.	Description of intelligent object design method. Remark networked design ap-proach/multidiscilinarity/complex ecosystem
3.	Group Activity: set groups, work)
4.	Summing up: difficulties found? Networked aspect? What other agents/actors they have left out? Why?

Some takeaways: intelligent agent, networked nature of intelligent systems, systems ans Extended Intelligence, the role of the user, the role of the designer

*Third Day: Machine Learning*  
Goals of the day:   
-	get to know the main types of machine learning
-	get to know the main tasks in learning from data: clustering, classification, prediction, then get and idea of the different types of methods (difference in what is learned and how it is learned)

1.	The role of machine learning in the design of intelligent objects (connection with Agent concept and feedback roots)
2.	Learning from data
3.	Main steps in learning form data
4.	Abstracted types of learning: from clustering to classification to prediction
5.	Abstraction from actual info (from weather to feelings): learning is more general + Exam-ples
6.	Basic procedures of ML: unsupervised/supervised
7.	Group Activity (identifiy the data your intelligent object may need to learn, find it, prepare it)
8.	Fast introduction to BigML
9.	Neural Nets  
	Intro to neural nets  
	Handwriting example  
	Convolutionary NNets  
	Group activity: discuss the interest of NN for your “Intelligent Object”

Some takeaways: generality of learning, specificity of methods, general idea about learning from data  

*Fourth Day : AI & Ethics*  
Goals of the day:    
-	grasp the politicial and ethical implications of AI
-	discuss the ethics of human and nonhuman entities
-	use Floridi’s framework
-	get to know the principal problems associated with ethical aspects: bias, exclusion, discrimina-tion, unfairness, etc. 

1.	Intelligent Agents, Political Agents, or Ethical Agents
2.	Onlife Manifesto: you are not your representation but your representation is you?. Floridi’s approach
3.	Power and Ethics implications of AI: from bias to discrimination
4.	Ethical Frameworks in AI. Dignum and others
5.	Group Activity: Cluster yourself (Magic Sauce) 20’
6.	Group Activity: anticipate the ethical complications of your designed intelligent object (40 minutes)
7.	Summing up: this lesson and the course. Next Steps

Some takeaways: posthuman design, ethical responsibility of designers of extended intelligen-ce systems  

### Output
The format of your work is quite open. Use the one that feel comfortable to express what you need to share: text, video, software, object, performance... 

### Grading Method
Individual work and presentation: 65%  
Quality of documentation (sources, argumentation, coherence): 35%  

### Bibliography
[A PROPOSAL FOR THE DARTMOUTH SUMMER RESEARCH PROJECT ON ARTIFICIAL INTELLIGENCE](http://web.archive.org/web/20170507180753/http://www-formal.stanford.edu/jmc/history/dartmouth/)  
[Stuart Russell and Peter Norving. Artificial Intelligence, a Modern Approach.](http://aima.cs.berkeley.edu/)  
[An Open Letter: Research Priorities for Robust and Beneficial Artificial Intelligence](https://futureoflife.org/ai-open-letter/)  
[Barcelona declaration for the proper development and usage of Artificial Intelligence in Europe](http://www.bdebate.org/sites/default/files/barcelona-declaration_v7-1-eng.pdf)  
[The 23 principles for AI of the Asilomar conference](https://futureoflife.org/ai-principles)  
Pasquale, Frank. 2015. The Black Box Society: The Secret Algorithms That Control Money and Information. Cambridge: Harvard University Press.  
Cathy O’Neill Weapons of Math Destruction: How Big Data Increases Inequality and Threatens Democracy. Crown Publishers  
[Joi Ito, Director of the MIT Media Lab on "Challenges of Extended Intelligence"](https://www.youtube.com/watch?v=KF9ZqnEiSzU)    
[Panel in World Economic Foum (Davos, 2017) on Artificial Intelligence with Microsoft CEO Nadella, IBM CEO Rometty and the director of the MIT Media Lab](https://thischangeseverything.org/book/)  
[Trends: the stanford study on AI 100 years](https://ai100.stanford.edu/) Long-term research on the development and impact of AI.    
[AI & Ethics](https://www.amazon.com/gp/product/0262720477?psc=1&redirect=true&ref_=oh_aui_detailpage_o01_s01)  
Luis Suarez-Villa, Technocapitalism: A Critical Perspective on Technological Innovation and Corporatism (Philadelphia: Temple University Press, 2009).  
Tiqqun. The Cybernetic Hypothesis. (2014). Accessed 15 Sept. 2017. 
[Richard Babbrook, “The Californian Ideology”](http://www.imaginaryfutures.net/2007/04/17/the-californian-ideology-2/)  
Adam Curtis. All Watched Over by Machines of Loving Grace (TV series)  
[Vladan Joler. Anatomy of an AI System.](https://anatomyof.ai/)  
Sara Córdoba, Wimer Hazenberg, Menno Huisman. (2011). Metaproducts. Meaningful Design For Our Connected World B/SPublishers  
M Kuniavski (2010). Smart Things: Ubiquitous Computing User Experience Design. Morgan Kaufmann. 
Frank Pasquale The Algorithmic Self (2015). The Hedgehog Review: Vol. 17 No. 1  
Adam Greenfield (2017). Radical Technologies  

### Background Research Material
Herbert Simon. The Sciences of the Artificial. 
Rodney Brook. Intelligence without Reason.  
Hubert Dreyfuss. What Computers Can't Do.  
Marvin Minsky. The Society of Mind. 
[Superintelligence: Paths Dangers, Strategies](https://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom/dp/1501227742)
[Grady Booch: Don't fear superintelligent AI](https://www.ted.com/talks/grady_booch_don_t_fear_superintelligence?language=en)  
[Jeremy Howard: The wonderful and terrifying implications of computers that can learn](https://www.ted.com/talks/)  
[Shyam Sankar: The rise of human-computer cooperation](https://www.ted.com/talks/shyam_sankar_the_rise_of_human_computer_cooperation/)  
[Breaking Walls Conference Berlin, 2016. BREAKING THE WALL TO LIVING ROBOTS. How Artificial Intelligence Research Tries to Build Intelligent Autonomous Systems](http://falling-walls.com/videos/Luc-Steels-1648)  
[The EDGE questions. 2015 : WHAT DO YOU THINK ABOUT MACHINES THAT THINK?](https://www.edge.org/responses/q2015)  
Jennifer Robertson. Robo Sapiens Japanicus: Robots, Gender, Family, and the Japanese Nation  
Laura Forlano. Posthumanism and Design. 

### Requirements for the Students
Be open and curious, question the current views on AI and Design and go forward from there (... and tinker a little with BigML machine learning online software platform)

### Infrastructure Needs
Projector, wifi, room space, some walls to post partial results of the projects going on at the course.

### RAMON SANGÜESA
![image](https://gitlab.com/MDEF/landing/raw/master/05425adc4b62-CV_FOTO_RamonSanguesa_S.jpg)

### Email Address
<rsanguesa@elisava.net>

### Personal Website
[equipocafeína](http://equipocafeina.net)

### Twitter Account
@ramonsang

### Professional BIO
Ramon Sangüesa has been doing research in Artificial Intelligence for more than 20 years specializing in Machine Learning, Uncertain Reasoning and Multiagent Systems. He is a professor at the Technical University of Catalonia (on academic leave) and now works as the Head of Technology Research at the Elisava Design and Engineering School. He has been visiting professor and researcher at Columbia University in NYC and Toronto University. He has been involved in Digital Social Innovation since long, being one of the creators of Citilab (Citizen's lab) in Barcelona and his first director of innovation. 

### LUCAS LORENZO PENA
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/1cc619b9ae25-IMG_5148.jpg)

### Email Address
<lucaslpena@gmail.com>

### Personal Website
[Lucas Lorenzo Pena](http://lucaslorenzopena.com )

### Twitter Account
@lucaslorenzop  

### Professional BIO
Lucas Pena helps innovative teams create, design, and deploy technology that impacts social experiences at the intersection of trust, governance, communities, and urban systems. 
He is a Full-Stack Software Engineer, UX Designer, and Cognitive Science & HCI Researcher currently fascinated by the symbiosis between humans and artificial intelligence as a means to empower new forms of transformative decision making.